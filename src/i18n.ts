import { createI18n } from 'vue-i18n'
import type { LocaleMessages } from '@intlify/core-base'

function loadLocaleMessages(): LocaleMessages<any> {
  const locales = require.context('./locales', true, /[A-Za-z0-9-_,\s]+\.json$/i)
  const messages: LocaleMessages<any> = {}
  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i)
    if (matched && matched.length > 1) {
      const locale = matched[1]
      messages[locale] = locales(key).default
    }
  })
  return messages
}

const missingKeys: { [key: string]: string } = {};

export default createI18n({
  legacy: false,
  locale: process.env.VUE_APP_I18N_LOCALE || 'en',
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en',
  messages: loadLocaleMessages(),
  missing: (locale, key) => {
    if (!(key in missingKeys)) {
      missingKeys[key] = key;
    }
    console.log('Missing keys:', JSON.stringify(missingKeys, null, 2));
  }
});
