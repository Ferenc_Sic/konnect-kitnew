import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '../app/views/HomeView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: HomeView
  },


  /**=================================**
   * SERVICES RELATED ROUTES SECTION
   **=================================**/
  {
    path: '/services',
    name: 'Services',
    component: () => import(/* webpackChunkName: "services" */ '../app/views/Services/index.vue')
  },
  {
    path: '/services/core-services',
    name: 'CoreServices',
    component: () => import(/* webpackChunkName: "services" */ '../app/views/Services/CoreServices/index.vue')
  },
  {
    path: '/services/education-and-training',
    name: 'EducationAndTraining',
    component: () => import(/* webpackChunkName: "services" */ '../app/views/Services/EducationAndTraining/index.vue')
  },
 
    // todo: sub core services
  {
    path: '/services/development-services',
    name: 'DevelopmentServices',
    component: () => import(/* webpackChunkName: "services" */ '../app/views/Services/DevelopmentServices/index.vue')
  },
    // todo: sub development services
  {
    path: '/services/artificial-intelligence-services',
    name: 'ArtificialIntelligenceServices',
    component: () => import(/* webpackChunkName: "services" */ '../app/views/Services/ArtificialIntelligenceServices/index.vue')
  },
    // todo: sub development services
  {
    path: '/services/automation-services',
    name: 'AutomationServices',
    component: () => import(/* webpackChunkName: "services" */ '../app/views/Services/AutomationServices/index.vue')
  },
    // todo: sub development services
  {
    path: '/services/data-services',
    name: 'DataServices',
    component: () => import(/* webpackChunkName: "services" */ '../app/views/Services/DataServices/index.vue')
  },
    // todo: sub development services
  {
    path: '/services/hosting-services',
    name: 'HostingServices',
    component: () => import(/* webpackChunkName: "services" */ '../app/views/Services/HostingServices/index.vue')
  },
    // todo: sub hosting services


  /**=================================**
   * SOLUTIONS RELATED ROUTES SECTION
   **=================================**/

  {
    path: '/solutions',
    name: 'Solutions',
    component: () => import(/* webpackChunkName: "solutions" */ '../app/views/Solutions/index.vue')
  },

  {
    path: '/solutions/engagement-solutions',
    name: 'EngagementSolutions',
    component: () => import(/* webpackChunkName: "solutions" */ '../app/views/Solutions/EngagementSolutions/index.vue')
  },

  {
    path: '/solutions/human-resources-solutions',
    name: 'HumanResourcesSolutions',
    component: () => import(/* webpackChunkName: "solutions" */ '../app/views/Solutions/HumanResourcesSolutions/index.vue')
  },

  {
    path: '/solutions/finance-solutions',
    name: 'FinanceSolutions',
    component: () => import(/* webpackChunkName: "solutions" */ '../app/views/Solutions/FinanceSolutions/index.vue')
  },

  {
    path: '/solutions/e-commerce-solutions',
    name: 'ECommerceSolutions',
    component: () => import(/* webpackChunkName: "solutions" */ '../app/views/Solutions/ECommerceSolutions/index.vue')
  },

  {
    path: '/solutions/custom-solutions',
    name: 'CustomSolutions',
    component: () => import(/* webpackChunkName: "solutions" */ '../app/views/Solutions/CustomSolutions/index.vue')
  },
  {
    path: '/solutions/data-solutions',
    name: 'DataSolutions',
    component: () => import(/* webpackChunkName: "solutions" */ '../app/views/Solutions/DataSolutions/index.vue')
  },
  {
    path: '/solutions/e-commerce-solutions',
    name: 'ECommerceSolutions',
    component: () => import(/* webpackChunkName: "solutions" */ '../app/views/Solutions/ECommerceSolutions/index.vue')
  },
  {
    path: '/solutions/custom-solutions',
    name: 'CustomSolutions',
    component: () => import(/* webpackChunkName: "solutions" */ '../app/views/Solutions/CustomSolutions/index.vue')
  },




  /**=================================**
   * SOLUTIONS RELATED ROUTES SECTION
   **=================================**/
  {
    path: '/developers',
    name: 'Developers',
    component: () => import(/* webpackChunkName: "developers" */ '../app/views/Developers/index.vue')
  },
  {
    path: '/developers/getting-started',
    name: 'DevelopersGettingStarted',
    component: () => import(/* webpackChunkName: "developers" */ '../app/views/Developers/GettingStarted/index.vue')
  },
  {
    path: '/developers/api-reference',
    name: 'DevelopersAPIReference',
    component: () => import(/* webpackChunkName: "developers" */ '../app/views/Developers/APIReference/index.vue')
  },
  {
    path: '/developers/documentation',
    name: 'DevelopersDocumentation',
    component: () => import(/* webpackChunkName: "developers" */ '../app/views/Developers/Documentation/index.vue')
  },
  {
    path: '/developers/sdks-n-libraries',
    name: 'DevelopersSDKsNLibraries',
    component: () => import(/* webpackChunkName: "developers" */ '../app/views/Developers/SDKsLibraries/index.vue')
  },
  {
    path: '/developers/code-samples',
    name: 'DevelopersCodeSamples',
    component: () => import(/* webpackChunkName: "developers" */ '../app/views/Developers/CodeSamples/index.vue')
  },
  {
    path: '/developers/utilities',
    name: 'DevelopersUtilities',
    component: () => import(/* webpackChunkName: "developers" */ '../app/views/Developers/Utilities/index.vue')
  },
  {
    path: '/developers/utilities/transcriptions',
    name: 'DevelopersTranscriptionUtility',
    component: () => import(/* webpackChunkName: "developers" */ '../app/views/Developers/Utilities/Transcriptions/index.vue')
  }, // other developer utilities... todo



  /**=================================**
   * SOLUTIONS RELATED ROUTES SECTION
   **=================================**/
  {
    path: '/community',
    name: 'Community',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/index.vue')
  },
  {
    path: '/community/announcements',
    name: 'CommunityAnnouncements',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Announcements/index.vue')
  },
  {
    path: '/community/blog',
    name: 'CommunityBlog',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Blog/index.vue')
  },
  {
    path: '/community/partnerships',
    name: 'CommunityPartnerships',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Partnerships/index.vue')
  },
  {
    path: '/community/f-a-q',
    name: 'CommunityFaq',
    component: () => import(/* webpackChunkName: "services" */ '../app/views/Community/FAQ/index.vue')
  },
  {
    path: '/community/blog/:slug',
    name: 'SingleBlog',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Blog/SingleBlog/index.vue')
},
  {
    path: '/community/events',
    name: 'CommunityEvents',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Events/index.vue')
  },
  // Entering Customers SubSection:
  {
    path: '/community/customers',
    name: 'CommunityCustomers',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Customers/index.vue')
  },
  {
    path: '/community/customers/sentinel-systems',
    name: 'CommunityCustomersSentinelSystems',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Customers/SentinelSystems/index.vue')
  },
  {
    path: '/community/customers/govwise',
    name: 'CommunityCustomersGovwise',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Customers/Govwise/index.vue')
  },
  {
    path: '/community/customers/bakkerij-broodhuys',
    name: 'CommunityCustomersBakkerijBroodhuys',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Customers/BakkerijBroodhuys/index.vue')
  },
  {
    path: '/community/customers/fractal-mind',
    name: 'CommunityCustomersFractalMind',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Customers/FractalMind/index.vue')
  },
  {
    path: '/community/customers/webwise',
    name: 'CommunityCustomersWebwise',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Customers/Webwise/index.vue')
  },
  // Entering Partners SubSection:
  {
    path: '/community/partners',
    name: 'CommunityPartners',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Partners/index.vue')
  },
  {
    path: '/community/partners/outsmartis',
    name: 'CommunityPartnersOutsmartis',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Partners/Outsmartis/index.vue')
  },
  {
    path: '/community/partners/detu-tech',
    name: 'CommunityPartnersDetutech',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Partners/Detutech/index.vue')
  },
  {
    path: '/community/partners/api-few',
    name: 'CommunityPartnersApifew',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Partners/Apifew/index.vue')
  },
  {
    path: '/community/partners/augmented-reality-tours',
    name: 'CommunityPartnersARTours',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Partners/ARTours/index.vue')
  },
  // Entering Investors SubSection:
  {
    path: '/community/investors',
    name: 'CommunityInvestors',
    component: () => import(/* webpackChunkName: "community" */ '../app/views/Community/Investors/index.vue')
  },


  /**=================================**
   * SOLUTIONS RELATED ROUTES SECTION
   **=================================**/
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/index.vue')
  },
  {
    path: '/about/company-overview',
    name: 'AboutCompanyOverview',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/CompanyOverview/index.vue')
  },
  {
    path: '/about/our-brands',
    name: 'AboutOurBrands',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/OurBrands/index.vue')
  },
  {
    path: '/about/our-brands/keycodemedia',
    name: 'AboutOurBrandsKeyCodeMedia',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/OurBrands/KeyCodeMedia/index.vue')
  },
  {
    path: '/about/our-brands/bewitt',
    name: 'AboutOurBrandsBewitt',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/OurBrands/BEWITT/index.vue')
  },
  {
    path: '/about/our-brands/easyapply',
    name: 'AboutOurBrandsEasyApply',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/OurBrands/EasyApply/index.vue')
  },

  {
    path: '/about/our-team',
    name: 'AboutOurTeam',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/OurTeam/index.vue')
  },
  {
    path: '/about/our-team/joao-morais',
    name: 'JoaoMorais',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/OurTeam/JoaoMorais/index.vue')
  },
  {
    path: '/about/our-team/shirley-martina',
    name: 'ShirleyMartina',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/OurTeam/ShirleyMartina/index.vue')
  },
  {
    path: '/about/our-team/ferenc-sic',
    name: 'FerencSic',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/OurTeam/FerencSic/index.vue')
  },
  {
    path: '/about/our-team/daan-balm',
    name: 'DaanBalm',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/OurTeam/DaanBalm/index.vue')
  },
  {
    path: '/about/our-team/dimitrije-djordjevic',
    name: 'DimitrijeDjordjevic',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/OurTeam/DimitrijeDjordjevic/index.vue')
  },
  {
    path: '/about/our-team/johnny-cartucho',
    name: 'JohnnyCartucho',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/OurTeam/JohnnyCartucho/index.vue')
  },
  {
    path: '/about/careers',
    name: 'AboutCareers',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/Careers/index.vue')
  },
  {
    path: '/about/contacts',
    name: 'AboutContacts',
    component: () => import(/* webpackChunkName: "about" */ '../app/views/About/Contacts/index.vue')
  },




]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
