import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import i18n from './i18n'

declare global {
    interface Window {
        gtag: any;
    }
}

const app = createApp(App).use(i18n);

router.afterEach((to, from) => {
  if (window.gtag) {
    window.gtag('config', 'G-QF1T5S6NWZ', {
      'page_path': to.path,
    });
  }
});

app.use(router).mount('#app');
